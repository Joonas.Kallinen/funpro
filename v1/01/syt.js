function syt(p, q) {
  //Jos q on 0, palauta p.
  if (q == 0) {
      return p;
  }
  //Muuten 
  else {
      //selvitä rekursiivisella metodikutsulla, mikä on suurin yhteinen tekijä q:lle ja p%q:lle,
      //ja palauta sama luku, jonka rekursiivinen kutsukin palautti.
      return syt(q,p%q);
  }
}
console.log('syt:');
console.log(syt(345,102)); // 3
console.log(syt(96,52)); // 4
console.log(syt(245,95)); // 5
console.log(syt(102,68)); // 34
console.log(syt(619,19)); // 1


function kjl(p, q) {
  if (q == 0) {
      if (p == 1) {
          return true;
      } else {
          return false;
      }
  }
  else {
      return kjl(q,p%q);
  }
}
console.log('kjl:');
console.log(kjl(345,102)); // false
console.log(kjl(96,52)); // false
console.log(kjl(245,95)); // false
console.log(kjl(102,68)); // false
console.log(kjl(619,19)); // true
console.log(kjl(35,18)); // true