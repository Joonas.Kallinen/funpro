function potenssiinKorotus(korotettava,korottaja) {
    if (korottaja == 0) {
        return 1;
    }
    else if (korottaja == 1)
        return korotettava;
    return korotettava * potenssiinKorotus(korotettava,korottaja-1);
}

console.log(potenssiinKorotus(2,1));
console.log(potenssiinKorotus(2,2));
console.log(potenssiinKorotus(2,3));
console.log(potenssiinKorotus(2,4));
console.log(potenssiinKorotus(2,5));
console.log(potenssiinKorotus(2,6));
console.log(potenssiinKorotus(2,7));
console.log(potenssiinKorotus(2,8));
console.log(potenssiinKorotus(2,9));
console.log(potenssiinKorotus(2,10));