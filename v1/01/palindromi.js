function onPalindromi(merkkijono) {
  //Jos merkkijonon pituus on 0 tai 1, palauta true.
  if (merkkijono.length == 0 || merkkijono.length == 1) {
      return true;
  }
  //Muuten jos merkkijonon ensimmäinen ja viimeinen merkki ovat erilaiset, palauta false.
  else {
      if (merkkijono.slice(0,1).toLowerCase() != merkkijono.slice(-1).toLowerCase()) {
          return false;
      } else {
          //Muissa tapauksissa ota jonon keskiosa, josta puuttuvat ensimmäinen ja viimeinen merkki,
          //selvitä rekursiivisella metodikutsulla, onko keskiosa palindromi, ja
          //palauta sama totuusarvo, jonka rekursiivinen kutsukin palautti.
          return onPalindromi(merkkijono.slice(1,-1));
      }
  }
}

console.log(onPalindromi('eiolepalindromi')); // false
console.log(onPalindromi('saippuakauppias')); // true
console.log(onPalindromi('Saippuakauppias')); // true
console.log(onPalindromi('a')); // true
console.log(onPalindromi('ab')); // false
console.log(onPalindromi('')); // true
console.log(onPalindromi('1234321')); // true
